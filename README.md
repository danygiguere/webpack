# Webpack compiler

Un template pour compiler vos fichier .scss et .js (ES6) à l'aide de Webpack.

### Installation:

- Veuillez d'abord entrez (dans le terminal  à la racine de ce projet) la commande `npm install` afin de télécharger les dépendances.


- Si vous créez de nouveaux fichiers .scss ou .js, veuillez les ajouter dans le module.exports du fichier `webpack.config.js`.

- Si vous créez de nouveaux dossiers css ou js, veuillez les ajoutez dans: ```new BrowserSyncPlugin({ files: ['./index.html', './css/*.css', './js/*.js']```

### Comment utiliser ce template:

Pour compiler vos assets, entrez (dans le terminal  à la racine de ce projet) la commande suivante:
```
npm run dev
```

Pour 'watcher' vos assets, entrez (dans le terminal  à la racine de ce projet) la commande suivante:
```
npm run watch
```
