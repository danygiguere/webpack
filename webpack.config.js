var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

var path = require('path');

var inProduction = (process.env.NODE_ENV === "production");

var BrowserSyncPlugin = require('browser-sync-webpack-plugin');

var configJs;
if (!inProduction) {
    configJs = {
        plugins: [
            new BrowserSyncPlugin({
                host: 'localhost',
                port: 8080,
                // proxy: 'http://localhost:8080',
                server: { baseDir: ['./'] },
                // files to watch, to reload the browser
                files: ['./index.html', './css/*.css', './js/*.js']
            }),
        ]
    }
}

var configScss = {
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: 'css-loader',
                            options: {url: false}
                        },
                        'sass-loader'
                    ],
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    }
};

/**
 * Compile the js file.
 *
 * @param {string} source - source of file with path and filename
 * @param {string} destination - destination path
 * @param {string} filename - destination filename
 */
function compileJs(source, destination, filename) {
    return Object.assign({}, configJs,{
        name: filename,
        entry: source,
        output: {
            path: destination,
            filename: filename + ".js"
        },
        optimization: {
            minimize: inProduction
        }
    })
}

/**
 * Compile the scss file.
 *
 * @param {string} source - source of file with path and filename
 * @param {string} destination - destination path
 * @param {string} filename - destination filename
 */
function compileScss(source, destination, filename) {
    return Object.assign({}, configScss, {
        name: filename,
        entry: source,
        output: {
            path: destination,
            filename: filename +'.css'
        },
        plugins: [
            new ExtractTextPlugin(filename +'.css'),
            new webpack.LoaderOptionsPlugin({
                minimize: inProduction
            })
        ]
    });
}

module.exports = [
    compileJs('./src/main.js', path.resolve(__dirname, 'js'), 'main'),
    compileJs('./src/other.js', path.resolve(__dirname, 'js'), 'other'),
    compileScss('./src/main.scss', path.resolve(__dirname, 'css'), 'main'),
    compileScss('./src/other.scss', path.resolve(__dirname, 'css'), 'other'),
];


